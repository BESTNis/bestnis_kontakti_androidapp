package com.example.android.bestniskontakti;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;
import android.widget.Toast;

import java.util.Arrays;

class ContactHelper {

    private static final String LOG_TAG = ContactHelper.class.getSimpleName();
    private static Intent mIntent;
    private static final Boolean[] checks = new Boolean[5];
    private static String[] toastStrings = {
            "Broj nije validan ili ne postoji",
            "Problem u Cursor klasi u Androidu"
    };
    private static Cursor cur;
    private static boolean commonCodeRet;
    private static boolean DEBUG;

    static void run(ContactModel contact, Activity _activity, int flag) {
        /* Flag = 1 - dodaje u kontakte
         * Flag = 2 - zove kontakt
         */
        DEBUG = ((BestNisAppClass) _activity.getApplication()).getDebugVariable();
        commonCodeRet = contactExists(contact.mobilePhone, _activity);

        if (flag == 1) {
            if (!commonCodeRet) {
                addToContacts(contact, _activity, 1);
            } else {
                addToContacts(contact, _activity, 0);
            }
        } else if (flag == 2) {
            callContact(contact, _activity);
        }
    }

    private static void sanitizeAndReturnIntent(ContactModel contact, Activity activity, int flag, int flag2) {
        mIntent = new Intent(ContactsContract.Intents.Insert.ACTION);
        mIntent.setType(ContactsContract.RawContacts.CONTENT_TYPE);

       /*
        *  i = 0 -> mobilePhone
        *  i = 1 -> name
        *  i = 2 -> surname
        *  i = 3 -> eMail
        *  i = 4 -> nickname
        *
        *  false if null
        */
        checks[0] = !(contact.mobilePhone == null);
        checks[1] = !(contact.name == null);
        checks[2] = !(contact.surname == null);
        checks[3] = !(contact.eMail == null);
        checks[4] = !(contact.nickname == null);

        if (checks[0]) {
            contact.mobilePhone = contact.mobilePhone.replaceAll("[^0-9]", "");
            checks[0] = (contact.mobilePhone.length() >= 9);
        }

        if (checks[3]) {
            checks[3] = contact.eMail.contains("@"); //true if contains @ char
        }

        if (DEBUG)
            Log.d(LOG_TAG, "Boolean[] check equals" + Arrays.toString(checks));

        //if phone is null break out
        if (checks[0]) {
            if (flag2 == 1) {
                if (flag == 0) {
                    mIntent = null;
                    commonCodeRet = commonCode(contact.mobilePhone, activity);
                    if (commonCodeRet) {
                        try {
                            if (cur.moveToFirst()) {
                                long contactID = cur.getLong(cur.getColumnIndexOrThrow(ContactsContract.PhoneLookup._ID));
                                Uri uri = Uri.withAppendedPath(ContactsContract.Contacts.CONTENT_URI,
                                        String.valueOf(contactID));

                                if (DEBUG)
                                    Log.d(LOG_TAG, "Uri lookupKey = " + uri.toString());


                                //close cursor
                                cur.close();

                                mIntent = new Intent(Intent.ACTION_VIEW);
                                mIntent.setData(uri);
                            }
                        } finally {
                            if (cur != null)
                                cur.close();
                        }
                    }
                } else {
                    mIntent.putExtra(ContactsContract.Intents.Insert.PHONE, contact.mobilePhone)
                            .putExtra(ContactsContract.Intents.Insert.PHONE_TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE);
                    if (checks[1] && checks[2]) {
                        mIntent.putExtra(ContactsContract.Intents.Insert.NAME,
                                contact.name + " " + contact.surname);
                    }

                    if (checks[3]) {
                        mIntent.putExtra(ContactsContract.
                                Intents.Insert.EMAIL_TYPE, ContactsContract.CommonDataKinds.Email.TYPE_WORK)
                                .putExtra(ContactsContract.Intents.Insert.EMAIL, contact.eMail);
                    }
                }
            } else if (flag2 == 2) {
                mIntent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + contact.mobilePhone));
            }

        } else {
            //don't show toast if problem was in commonCode
            if (commonCodeRet) {
                Toast.makeText(activity.getBaseContext(),
                        toastStrings[0], Toast.LENGTH_LONG).show();
            }
            mIntent = null;
        }
    }

    /* common code for Cursor based methods */
    private static boolean commonCode(String number, Activity activity) {
        //this should never happen, but add check in case it does
        if (number == null) {
            if (DEBUG)
                Log.d(LOG_TAG, "Number is null");

            return false;
        }
        //this can happen due to Portal not checking when HR or admin enters new users

        //isEmpty returns false? maybe string is blank
        //but length returned is 0 when there is no phone
        if (number.isEmpty()) {
            if (DEBUG)
               Log.d(LOG_TAG, "Number is empty");

            Toast.makeText(activity.getBaseContext(),
                    toastStrings[0], Toast.LENGTH_LONG).show();
            return false;
        }

        if (DEBUG)
            Log.d(LOG_TAG, "Number = " + number);


        Uri lookupUri = Uri.withAppendedPath(
                ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));

        String[] mPhoneNumberProjection = {
                ContactsContract.PhoneLookup._ID,
                ContactsContract.PhoneLookup.NUMBER,
                ContactsContract.PhoneLookup.DISPLAY_NAME
        };

        try {
            cur = activity.getContentResolver().query(
                    lookupUri, mPhoneNumberProjection, null, null, null);
            return true;
        } catch (Exception e) {
            Toast.makeText(activity.getBaseContext(),
                    toastStrings[1], Toast.LENGTH_LONG).show();
            Log.e(LOG_TAG, "Exception: " + e);
            return false;
        }

    }

    private static boolean contactExists(String number, Activity activity) {
        if (number != null) {
            if(commonCode(number, activity)) {
                    try {
                        if (cur.moveToFirst()) {
                            return true;
                        }
                    } finally {
                        if (cur != null)
                            cur.close();
                    }
            }
            return false;
        } else {
            return false;
        }

    }

    private static void addToContacts(ContactModel contact, Activity activity, int flag) {
        sanitizeAndReturnIntent(contact, activity, flag, 1); /* poslednji param == 1 - dodaje u kontakte*/

        if (!(mIntent == null)) {
            activity.startActivity(mIntent);
        }
    }

    private static void callContact(ContactModel contact, Activity activity) {
        sanitizeAndReturnIntent(contact, activity, 0, 2); /* poslednji param == 2 - zove kontakt, pretposlednji param nije bitan */

        if (!(mIntent == null)) {
            activity.startActivity(mIntent);
        }
    }
}
