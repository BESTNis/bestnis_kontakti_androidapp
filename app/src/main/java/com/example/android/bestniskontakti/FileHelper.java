package com.example.android.bestniskontakti;

import android.content.Context;
import android.util.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;


public class FileHelper {

    private static final String LOG_TAG = FileHelper.class.getSimpleName();


    public static String readStringFromFile(Context context, String localfilename, boolean DEBUG) {
        String ret = "";
        try {
            InputStream inputStream = context.openFileInput(localfilename);

            if ( inputStream != null ) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);

                String line;
                StringBuilder stringBuilder = new StringBuilder();

                while ( (line = bufferedReader.readLine()) != null ) {
                    stringBuilder.append(line);
                }

                inputStream.close();
                String loadedstream = stringBuilder.toString();

                if (DEBUG)
                    Log.d(LOG_TAG, "String loadedstream = " + loadedstream);

                ret = loadedstream;
            }
        }
        catch (FileNotFoundException e) {
            Log.e(LOG_TAG, "readStringFromFile: File not found: " + e.toString());
            ret = "";
        } catch (IOException e) {
            Log.e(LOG_TAG, "readStringFromFile: Can not read file: " + e.toString());
            ret = "";
        }
        return ret;
    }

    public static boolean writeStringToFile(Context context, String localfilename, String data, boolean DEBUG) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
                    context.openFileOutput(localfilename,
                            Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();

            if (DEBUG)
                Log.d(LOG_TAG, "writeStringToFile: String data = " + data);

            return true;
        }
        catch (IOException e) {
            Log.e(LOG_TAG, "writeStringToFile: File write failed: " + e.toString());
            return false;
        }
    }

    public static boolean checkAuthData(Context context, String localfilename, String email, String password, boolean DEBUG, String splittag) {
        String ret = readStringFromFile(context, localfilename, DEBUG);
        String[] authData = new String[4];
        if (!ret.isEmpty()) {

            if (DEBUG)
                Log.d(LOG_TAG, "String ret = " + ret);

            authData = ret.split(splittag);

            if (DEBUG) {
                int i = 0;
                for (String authString : authData) {
                    Log.d(LOG_TAG, "auth[" + i + "] = " + authString);
                    i++;
                }
                Log.d(LOG_TAG, "email = " + authData[0]);
                Log.d(LOG_TAG, "password = " + authData[1]);
            }

            //if authData length is 2 and both strings equal to their counterparts this will be true
            return (authData.length == 2) && (authData[0].equals(email) && authData[1].equals(password));

        } else {
            if (DEBUG)
                Log.d(LOG_TAG, "String ret is empty");
            return false;
        }
    }

    public static boolean fileExists(Context context, String filename) {
        File file = context.getFileStreamPath(filename);
        return (!(file == null || !file.exists()));
    }

    public static String[] savedAuthDataReturn(Context context, String localfilename, boolean DEBUG, String splittag) {
        String ret = readStringFromFile(context, localfilename, DEBUG);
        String[] authData = new String[4];

        authData = ret.split(splittag); /* [0] - email, [1] - password */

        if (DEBUG)
            Log.d(LOG_TAG, "savedAuthDataReturn: authData = " + authData[0] + authData[1]);

        return authData;
    }
}
