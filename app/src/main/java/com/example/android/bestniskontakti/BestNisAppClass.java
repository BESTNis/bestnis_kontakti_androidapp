package com.example.android.bestniskontakti;

import android.app.Application;

/**
 * I'm using this for App global vars
 */
public class BestNisAppClass extends Application {

    private boolean DEBUG = false;

    public boolean getDebugVariable() {
        return DEBUG;
    }

    //not used for now
    public void setVariable(boolean DEBUG) {
            this.DEBUG = DEBUG;
    }

}
