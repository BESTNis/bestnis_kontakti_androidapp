package com.example.android.bestniskontakti;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class ContactListActivity extends AppCompatActivity {

    private PopulateArrayAdapter populateTask;
    EditText inputSearch;
    private ListView lv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_list);

        populateTask = new PopulateArrayAdapter(
                getIntent().getExtras().getString(Intent.EXTRA_TEXT));
        populateTask.execute();

        lv = (ListView) findViewById(R.id.listview_contacts);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        lv.setScrollingCacheEnabled(true);
        //for SectionIndexer fancy alphabetical scroll
        lv.setFastScrollEnabled(true);

        populateTask = null;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent start = new Intent(this, LoginActivity.class);
        startActivity(start);
        finish();
    }


    /* this is due to very large populate data, so UI thread wouldn't freeze */
    public class PopulateArrayAdapter extends AsyncTask<Void, Void, Void> {
        private final String mContacts;
        private ContactsArrayAdapter contactModelArrayAdapter;

        PopulateArrayAdapter(String contactsJSON) {
            this.mContacts = contactsJSON;
        }

        @Override
        protected Void doInBackground(Void... params) {
            ArrayList<ContactModel> ContactArrayList = ContactParser.parseToArrayAdapter(this.mContacts);
            contactModelArrayAdapter = new ContactsArrayAdapter(getBaseContext(), ContactArrayList);

            //set search
            inputSearch = (EditText) findViewById(R.id.search_contacts);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            lv.setTextFilterEnabled(true);

            lv.setAdapter(contactModelArrayAdapter);

            //add to contacts on long click
            lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    //ContactHelper will do all the work
                    int dodaj_kontakt = 1;
                    ContactHelper.run(contactModelArrayAdapter.getItem(position),
                            ContactListActivity.this, dodaj_kontakt);
                    return true;
                }
            });


            // Create a ListView-specific touch listener. ListViews are given special treatment because
            // by default they handle touches for their list items... i.e. they're in charge of drawing
            // the pressed state (the list selector), handling list item clicks, etc.
            SwipeListViewTouchListener touchListener =
                    new SwipeListViewTouchListener(
                            lv,
                            new SwipeListViewTouchListener.OnSwipeCallback() {
                                int pozovi_kontakt = 2;
                                @Override
                                public void onSwipeLeft(ListView listView, int [] reverseSortedPositions) {
                                    //  Log.i(this.getClass().getName(), "swipe left : pos="+reverseSortedPositions[0]);
                                    // TODO : YOUR CODE HERE FOR LEFT ACTION
                                    final ContactModel item;
                                    item = contactModelArrayAdapter.getItem(reverseSortedPositions[0]);
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            ContactHelper.run(item, ContactListActivity.this, pozovi_kontakt);
                                        }
                                    }).start();

                                }

                                @Override
                                public void onSwipeRight(ListView listView, int [] reverseSortedPositions) {
                                    //  Log.i(ProfileMenuActivity.class.getClass().getName(), "swipe right : pos="+reverseSortedPositions[0]);
                                    // TODO : YOUR CODE HERE FOR RIGHT ACTION
                                }
                            },
                            true, // example : left action = dismiss
                            false); // example : right action without dismiss animation
            lv.setOnTouchListener(touchListener);
            // Setting this scroll listener is required to ensure that during ListView scrolling,
            // we don't look for swipes.

            lv.setOnScrollListener(touchListener.makeScrollListener());

            inputSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    contactModelArrayAdapter.getFilter().filter(charSequence);
                    //contactModelArrayAdapter.notifyDataSetChanged();
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }
    }

}
