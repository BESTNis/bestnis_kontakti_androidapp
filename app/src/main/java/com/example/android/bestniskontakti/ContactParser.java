package com.example.android.bestniskontakti;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;


//parse contacts to arraylist
public class ContactParser {

    public static ArrayList<ContactModel> parseToArrayAdapter(String contactsJSON) {
        Gson gson = new Gson();

        Type collectionType = new TypeToken<ArrayList<ContactModel>>() {
        }.getType();
        ArrayList<ContactModel> contactsArrayList = gson.fromJson(contactsJSON, collectionType);

        return contactsArrayList;
    }
}
