package com.example.android.bestniskontakti;

import com.google.gson.annotations.SerializedName;

public class ContactModel {

    @SerializedName("ime")
    public String name;

    @SerializedName("prezime")
    public String surname;

    @SerializedName("nadimak")
    public String nickname;

    @SerializedName("brMobil")
    public String mobilePhone;

    @SerializedName("eMail")
    public String eMail;

    // I need this for generic filtering class for ArrayAdapter
    //Add surname to be first due to Sectionindexer implementation
    @Override
    public String toString() {
        return new StringBuilder()
                .append(this.surname)
                .append(this.name)
                .append(this.nickname)
                .append(this.eMail)
                .append(this.mobilePhone)
                .toString();
    }
}
