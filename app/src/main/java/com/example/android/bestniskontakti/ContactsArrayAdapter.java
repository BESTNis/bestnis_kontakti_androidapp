package com.example.android.bestniskontakti;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.SectionIndexer;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Set;


public class ContactsArrayAdapter extends ArrayAdapter<ContactModel> implements SectionIndexer {
    private final ArrayList<ContactModel> contacts;
    //private Context context;
    private Filter filter;
    private HashMap<String, Integer> alphaIndexer;
    private String[] sections = new String[0];
    private static final String serbianLatinChars = "ŠĐČĆŽšđčćž";
    private static final List<String> serbianAlphabet = Arrays.asList(
            "A", "B", "C", "Č", "Ć", "D",
            "Đ", "E", "F", "G", "H", "I",
            "J", "K", "L", "M", "N", "O",
            "P", "R", "S", "Š", "T", "U",
            "V", "Z", "Ž"
    );
    private static ViewHolder viewHolder;




    public ContactsArrayAdapter(Context context, final ArrayList<ContactModel> contacts) {
        super(context, 0, contacts);
        //this.context = context;
        this.contacts = contacts;

        //setup sectionadapter in new thread to rebalance load
        new Thread(new Runnable() {
            @Override
            public void run() {
                setupSectionAdapter();
            }
        }).start();
    }

    @Override
    public void notifyDataSetChanged () {
        super.notifyDataSetChanged();

        /*
        * when dataset is changed we must run again setupSectionAdapter()
        * for new ordering and it MUST be run on UI thread now due to
        * ArrayAdapter being changed on fly while set to ListView.
        * */
        setupSectionAdapter();
    }

    public void setupSectionAdapter() {
        alphaIndexer = new HashMap<String, Integer>();

        for(int i = contacts.size() - 1; i >= 0; i--)
        {
            ContactModel element = contacts.get(i);
            String firstChar = element.toString().substring(0, 1).toUpperCase();

            //check characters
            if(firstChar.charAt(0) > 'Z' || firstChar.charAt(0) < 'A') {
                if (!serbianLatinChars.contains(firstChar))
                    firstChar = "@";
            }

            //put our firs char (first char of surname is returned of contact element)
            //in HashMap and map it to i-th element from contacts ArrayList
            if (!alphaIndexer.containsKey(firstChar))
                alphaIndexer.put(firstChar, i);
        }

        //make set of keys
        Set<String> keys = alphaIndexer.keySet();
        //make iterator
        Iterator<String> it = keys.iterator();

        //now make ArrayList container that will be populated with keys
        ArrayList<String> keyList = new ArrayList<String>();

        //Iterate through all keys and add it to keyList
        while(it.hasNext())
            keyList.add(it.next());

        //sort keys by serbian latin alphabet
        Collections.sort(keyList, new Comparator<String>() {
            @Override
            public int compare(String lhs, String rhs) {
                //check indexes of serbian alphabet
                return Integer.valueOf(
                        serbianAlphabet.indexOf(lhs))
                        .compareTo(
                                Integer.valueOf(
                                        serbianAlphabet.indexOf(rhs)));
            }
        });

        //instantiate global var sections and populate it with keyList values
        sections = new String[keyList.size()];
        keyList.toArray(sections);
    }
    public int getPositionForSection(int section)
    {
        String letter = sections[section];

        return alphaIndexer.get(letter);
    }

    public int getSectionForPosition(int position)
    {
        return 0;
    }

    public Object[] getSections()
    {
        return sections;
    }

    private static class ViewHolder {
        TextView name;
        TextView surname;
        TextView nickname;
        TextView mobilePhone;
        TextView email;
        TextView poziv;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        // Get the data item for this position
        ContactModel contact = getItem(position);
        Resources res = getContext().getResources();

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_contact, parent, false);

            viewHolder.name = (TextView) convertView.findViewById(R.id.contact_name);
            viewHolder.surname = (TextView) convertView.findViewById(R.id.contact_surname);
            viewHolder.nickname = (TextView) convertView.findViewById(R.id.contact_nickname);
            viewHolder.mobilePhone = (TextView) convertView.findViewById(R.id.contact_mobile);
            viewHolder.email = (TextView) convertView.findViewById(R.id.contact_email);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        // Get all data from each ContactModel
        viewHolder.name.setText(String.format(res.getString(R.string.name), contact.name));
        viewHolder.surname.setText(String.format(res.getString(R.string.surname), contact.surname));
        viewHolder.nickname.setText(String.format(res.getString(R.string.nickname), contact.nickname));
        viewHolder.mobilePhone.setText(String.format(res.getString(R.string.mobile), contact.mobilePhone));
        viewHolder.email.setText(String.format(res.getString(R.string.email), contact.eMail));

        // Return view to render
        return convertView;
    }

    @Override
    public Filter getFilter() {
        if (filter == null)
            filter = new AppFilter<>(contacts);
        return filter;

    }

    /**
     * Class for filtering in Arraylist listview. Objects need a valid
     * 'toString()' method.
     *
     * @author Tobias Schürg inspired by Alxandr
     *         (http://stackoverflow.com/a/2726348/570168)
     *
     * @version 1.1 - modified by Dusan Knezevic
     *
     *  Added option to filter via more string arguments
     *  and that object needs to match all string arguments
     *  Example: "Name Surname" --> Object needs to contain both
     *  name and surname substrings. Name and surname are being treated
     *  as separate strings. You can have any number of string/substring arguments.
     */
    private class AppFilter<T> extends Filter {

        private ArrayList<T> sourceObjects;

        public AppFilter(List<T> objects) {
            sourceObjects = new ArrayList<T>();
            synchronized (this) {
                sourceObjects.addAll(objects);
            }
        }


        @Override
        protected FilterResults performFiltering(CharSequence chars) {
            String filterSeq = chars.toString().toLowerCase();
            FilterResults result = new FilterResults();
            if (filterSeq.length() > 0) {
                ArrayList<T> filter = new ArrayList<T>();

                //parse strings by empty space, tab... etc.
                String[] filterSeqs = filterSeq.split("\\s+");
                //number of filtering string params
                int numFilterStrings = filterSeqs.length;
                for (T object : sourceObjects) {

                    //get number of strings that match in object
                    int j = 0;

                    for (String filterString : filterSeqs) {
                        if (object.toString().toLowerCase().contains(filterString)) {
                            j++;
                        }
                    }

                    // if all search strings match, add object
                    if (j == numFilterStrings) {
                        filter.add(object);
                    }
                }
                result.count = filter.size();
                result.values = filter;
            } else {
                // add all objects
                synchronized (this) {
                    result.values = sourceObjects;
                    result.count = sourceObjects.size();
                }
            }
            return result;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            // NOTE: this function is *always* called from the UI thread.
            ArrayList<T> filtered = (ArrayList<T>) results.values;
            notifyDataSetChanged();
            clear();

            for (T filteredSingle : filtered) {
                add((ContactModel) filteredSingle);
            }

            notifyDataSetInvalidated();
        }
    }


}
