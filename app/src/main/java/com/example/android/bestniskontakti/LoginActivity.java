package com.example.android.bestniskontakti;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.annotation.TargetApi;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;


public class LoginActivity extends AppCompatActivity {

    /**
     * Keep track of the login task to ensure we can cancel it if requested.
     */
    private UserLoginTask mAuthTask = null;

    /**
     * Save JSON return from AsyncTask
     */
    private String contactsJSON;

    /**
     * File name where to save local JSON
     */

    private static final String LOCAL_FILENAME_JSON = "localJSON.json";

    /**
     * File name where to save login data
     */
    private static final String LOCAL_FILENAME_AUTH = "authData.txt";

    /**
     * String splitting tag. Used for string split methods
     */
    private static final String STRING_SPLIT_TAG = ",,,,";

    /**
     * Log tag for logging purposes
     */
    private static final String LOG_TAG = LoginActivity.class.getSimpleName();

    /**
     * boolean for debug logging
     * if true all Log.* that are not Log.e will also be executed
     * Log.e will always be executed on errors
     */
    private static boolean DEBUG;

    /**
     * email and password strings needed for mAuthTask down
     */
    private String email;
    private String password;

    /**
     * String array of strings to be shown on toasts
     */
    private static final String[] toastStrings = {
            "Greška prilikom kontaktiranja servera",
            "Greška na serveru: Server nije vratio nikakve podatke",
            "Pogrešna lozinka ili mail",
            "Netačni login podaci."
                    + "\n" +
                    " Morate koristi vaš nalog kada niste na internetu",
            "Niste na internetu. Učitavam keširane podatke...",
            "Morate se prvi put konektovati na internet kako bi se keširali podaci na uređaju",
            "Nema keširanih login podataka na uređaju"
    };


    // UI references.
    private AutoCompleteTextView mEmailView;
    private EditText mPasswordView;
    private View mProgressView;
    private View mLoginFormView;
    private Button mEmailSignInButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        DEBUG = ((BestNisAppClass) this.getApplication()).getDebugVariable();

        setContentView(R.layout.activity_login);
        // Set up the login form.
        mEmailView = (AutoCompleteTextView) findViewById(R.id.email);

        mPasswordView = (EditText) findViewById(R.id.password);

        mPasswordView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        mEmailSignInButton = (Button) findViewById(R.id.email_sign_in_button);


        mLoginFormView = findViewById(R.id.login_form);
        mProgressView = findViewById(R.id.login_progress);
    }

    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mPasswordView.setText("");
        mEmailView.setText("");

        mEmailSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                email = mEmailView.getText().toString();
                password = mPasswordView.getText().toString();
                attemptLogin();
            }
        });

        getDelegate().onPostCreate(savedInstanceState);

        autoCompleteSavedAuthData();
    }


    private void autoCompleteSavedAuthData() {
        if (FileHelper.fileExists(LoginActivity.this, LOCAL_FILENAME_AUTH)) {
            String[] authData =
                    FileHelper.savedAuthDataReturn(LoginActivity.this, LOCAL_FILENAME_AUTH, DEBUG, STRING_SPLIT_TAG);

            if (DEBUG)
                Log.d(LOG_TAG, "autoCompleteSavedAuthData: authData = " + authData[0] + authData[1]);

            mEmailView.setText((String) authData[0]);
            mPasswordView.setText((String) authData[1]);
        } else {
            Toast.makeText(LoginActivity.this, toastStrings[6], Toast.LENGTH_LONG).show();
        }
    }

    private void attemptLogin() {
        if ((ConnectionCheck.getConnectivityStatus(getBaseContext()) == ConnectionCheck.TYPE_WIFI)
                || (ConnectionCheck.getConnectivityStatus(getBaseContext()) == ConnectionCheck.TYPE_MOBILE)) {

            if (mAuthTask != null) {
                return;
            }

            // Reset errors.
            mEmailView.setError(null);
            mPasswordView.setError(null);

            boolean cancel = false;
            View focusView = null;

            // Check for a valid password, if the user entered one.
            if (!TextUtils.isEmpty(password) && !isPasswordValid(password)) {
                mPasswordView.setError(getString(R.string.error_invalid_password));
                focusView = mPasswordView;
                cancel = true;
            }

            // Check for a valid email address.
            if (TextUtils.isEmpty(email)) {
                mEmailView.setError(getString(R.string.error_field_required));
                focusView = mEmailView;
                cancel = true;
            } else if (!isEmailValid(email)) {
                mEmailView.setError(getString(R.string.error_invalid_email));
                focusView = mEmailView;
                cancel = true;
            }

            if (cancel) {
                // There was an error; don't attempt login and focus the first
                // form field with an error.
                focusView.requestFocus();
            } else {
                // Show a progress spinner, and kick off a background task to
                // perform the user login attempt.
                showProgress(true);
                mAuthTask = new UserLoginTask(email, password);
                mAuthTask.execute();
            }
        } else {
            //start this in new thread to make it work
            LoadFromDevice load = new LoadFromDevice();
            load.execute();
        }
    }


    private boolean isEmailValid(String email) {
        return email.contains("@");
    }

    private boolean isPasswordValid(String password) {
        return password.length() > 4;
    }


    @TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
    private void showProgress(final boolean show) {
        // On Honeycomb MR2 we have the ViewPropertyAnimator APIs, which allow
        // for very easy animations. If available, use these APIs to fade-in
        // the progress spinner.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB_MR2) {
            int shortAnimTime = getResources().getInteger(android.R.integer.config_shortAnimTime);

            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
            mLoginFormView.animate().setDuration(shortAnimTime).alpha(
                    show ? 0 : 1).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
                }
            });

            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mProgressView.animate().setDuration(shortAnimTime).alpha(
                    show ? 1 : 0).setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
                }
            });
        } else {
            // The ViewPropertyAnimator APIs are not available, so simply show
            // and hide the relevant UI components.
            mProgressView.setVisibility(show ? View.VISIBLE : View.GONE);
            mLoginFormView.setVisibility(show ? View.GONE : View.VISIBLE);
        }
    }

    private void contactListActivityStart() {
        Intent intent = new Intent(LoginActivity.this, ContactListActivity.class);
        intent.putExtra(Intent.EXTRA_TEXT, LoginActivity.this.contactsJSON);
        startActivity(intent);
        //finish this activity
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mAuthTask = null;
        finish();
    }

    /**
     * Represents an asynchronous login/registration task used to authenticate
     * the user.
     */
    public class UserLoginTask extends AsyncTask<Void, Void, String> {

        private final String mEmail;
        private final String mPassword;
        private final String LOG_TAG = UserLoginTask.class.getSimpleName();

        UserLoginTask(String email, String password) {
            mEmail = email;
            mPassword = password;
        }

        @Override
        protected String doInBackground(Void... params) {

            HttpURLConnection urlConnection = null;
            BufferedReader reader = null;


            // Will contain the raw JSON response as a string.
            String response = null;
            try {

                Uri.Builder urlBuilder = new Uri.Builder();
                urlBuilder.scheme("http")
                        .authority("jobfairnis.rs")
                        .appendPath("portal_dev")
                        .appendPath("android_kontakti_api")
                        .appendPath("listaClanova.php")
                        .appendQueryParameter("user", mEmail)
                        .appendQueryParameter("password", mPassword);

                URL url = new URL(urlBuilder.build().toString());

                urlConnection = (HttpURLConnection) url.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                //Read input stream
                InputStream inputStream = urlConnection.getInputStream();
                StringBuilder buffer = new StringBuilder();

                if (inputStream == null) {
                    return null;
                }
                reader = new BufferedReader(new InputStreamReader(inputStream));

                /*
                    Read each response line from jobfairnis server
                 */
                String line;
                while ((line = reader.readLine()) != null) {
                    buffer.append(line).append("\n");
                }

                if (buffer.length() == 0) {
                    return null;
                }

                response = buffer.toString();


            } catch (IOException e) {

                Log.e(LOG_TAG, "Error: ", e);
                return null;

            } finally {

                if (urlConnection != null) {
                    urlConnection.disconnect();
                }
                if (reader != null) {
                    try {
                        reader.close();
                    } catch (final IOException e) {
                        Log.e(LOG_TAG, "Error closing stream", e);
                    }
                }
            }

            return response;
        }

        @Override
        protected void onPostExecute(String responseJSON) {
            super.onPostExecute(responseJSON);

            mAuthTask = null;
            if (null != responseJSON) {
                if (!responseJSON.isEmpty()) {
                    if (!responseJSON.contains("false")) {

                        LoginActivity.this.contactsJSON = responseJSON;

                        String _email = (LoginActivity.this.email == null || LoginActivity.this.email.isEmpty())
                                ? email : LoginActivity.this.email;
                        String _password = (LoginActivity.this.password == null || LoginActivity.this.password.isEmpty())
                                ? password : LoginActivity.this.password;

                        boolean ret = FileHelper.writeStringToFile(LoginActivity.this, LOCAL_FILENAME_AUTH,
                                _email + STRING_SPLIT_TAG + _password,
                                DEBUG);


                        if (DEBUG)
                            Log.d(LOG_TAG, "writeStringToFile(AUTH) ret = " + ret);

                        ret = FileHelper.writeStringToFile(
                                LoginActivity.this,
                                LoginActivity.LOCAL_FILENAME_JSON,
                                responseJSON, DEBUG);

                        if (DEBUG)
                            Log.d(LOG_TAG, "writeJSONtoFile(JSON) returned: " + ret);

                        contactListActivityStart();
                        showProgress(false);
                    } else {
                        showProgress(false);
                        mPasswordView.setError(getString(R.string.error_incorrect_password));
                        mEmailView.setError(getString(R.string.error_invalid_email));
                        mEmailView.requestFocus();
                        mPasswordView.requestFocus();
                        Toast.makeText(getBaseContext(),
                                toastStrings[2], Toast.LENGTH_LONG).show();
                    }
                } else {
                    showProgress(false);
                    Toast.makeText(getBaseContext(),
                            toastStrings[1], Toast.LENGTH_LONG).show();
                }
            } else {
                showProgress(false);
                Toast.makeText(getBaseContext(),
                        toastStrings[0], Toast.LENGTH_LONG).show();
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

            mAuthTask = null;
            showProgress(false);
        }
    }
    /**
     End of UserLoginTask (mAuthTask) global var
     */

    /**
     * Loading JSON and checking auth data from device
     */
    private class LoadFromDevice extends AsyncTask<Void, Void, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mAuthTask = null;
            showProgress(true);
        }

        @Override
        protected String doInBackground(Void... params) {
            String contactsJSON;

            if (FileHelper.checkAuthData(LoginActivity.this,
                    LOCAL_FILENAME_AUTH, email, password,
                    DEBUG, STRING_SPLIT_TAG)) {
                contactsJSON =
                        FileHelper.readStringFromFile(LoginActivity.this, LOCAL_FILENAME_JSON, DEBUG);
            } else {
                contactsJSON = null;
            }

            return contactsJSON;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            showProgress(false);
            if (null != result) {
                LoginActivity.this.contactsJSON = result;
                LoginActivity.this.contactListActivityStart();
            } else {
                Toast.makeText(getBaseContext(), toastStrings[3],
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}

